use std::{
    io,
    vec,
    collections::HashMap,
    cmp::Ordering
};
use rand::Rng;

fn main() {
    let a_indexes = "abcd"; // Şık isimleri.
    let mut questions = HashMap::new(); //(soru, [(cevap, solcu_değeri), (cevap_0, solcu_değeri), ..., (cevap_n-1, solcu_değeri)] ) biçiminde ilerleyen bir tuple.
    let mut result: i8 = 0;
    let mut q_indexes: Vec<u8> = vec![1, 2, 3, 4]; // Soruların alabileceği numara değerlerinin bir listesi.

    // Soruları HashMap'e ekle. (soru_numarası, (soru, [(cevap_1, değer), (cevap_2, değer), ...]))
    questions.insert(1, ("Solcu musun?",
        [("muhtemel", 2), ("yok", 1), ("reddediyorum", 0), ("marx > allah", 3)]
    ));

    questions.insert(2, ("Favori Sovyet Sosyalist Cumhuriyetler Birliği tankın hangisi?",
        [("Panzerkampfwagen WG-3004", -2), ("PT-76", 1), ("T-62", 3), ("T-34", 1)]
    ));
    
    questions.insert(3, ("\"Allah...\"",
        [("var", 0), ("aaaaasdv,çcx.asğ,cxvb,,,,as,fvd,", 2), ("yok", 3), ("level 10 gyatt", 1)]
    ));
    
    questions.insert(4, ("Bir tanıdığın seni, kahve ısmarlama sözü vererek bilmediğin bir restorana çağırdı. Mekâna girdiğin an, fiyatın da ananın amına gireceğini anlamış bulundun. Etrafta zengin fantezileri dönüyor, Jeff Bezos tipli adamlar milisaniyede 1 kez şampanya patlatıyor falan, o derece.\nParan yok; arkadaşın olmazsa, aldığın herhangi bir şeyin fiyatını dürülmüş banknotlar biçiminde sana sokarlar.", 
        [ ("Siparişin gelmeden restorandan fırla.", 2), ("\"hasan.seccegin.restorani.sikeyim.amin.feryadi.tkp_ml.genclik.kollarindan.atildin\"", 3), ("Aslında... Yemekleri de fena değil gibi. Kırk yılda bir adam gibi yemek yiyorsun, otursan bir şey olmaz.", 1), ("Saçını kazıyıp Jeff Bezos klonlarına katıl.", -2)]
    ));

    println!("<-< Solcu testine hoş geldin. >->\n");
    
    let mut i = 1; // Arttırılarak soruların başına eklemek üzere bir integer oluştur. KULLANMAM GEREKTİĞİNE EMİN DEĞİLİM.
    for _ in &questions {
        let randq = &questions[&random_index(&mut q_indexes)]; // Rastgele soru seç ve değişkene ata.
        let mut answer;

            println!("{}. {}\n", i.to_string(), &randq.0); // Soruyu numarasıyla beraber yazdır.
            
            for x in 0..=3 { // Her soruda sabit olan 4 cevabın yazdırılması için, 4 kez tekrar eden bir döngü.
                let char = match a_indexes.chars().nth(x) { // Hazır karakterlerden doğru olanı, x'inciyi al.
                    Some(n) => n,
                    None => ' '
                };                

                println!("  {}) {}", char, randq.1[x].0); // Şıkkı yazdır.
                // randq.1 = sorunun (tuple) cevaplar bölümü [array],
                // randq.1[x] = x'inci cevap (tuple)
                // randq.1[x].0 = x'inci cevabın (tuple), eh, cevap kısmı. (Cevap tuple'ının ikinci kısmı ise cevabın sonuca etkisi.)
            }
            
            loop {
                answer = String::new();
                
                io::stdin()
                    .read_line(&mut answer)
                    .expect("vereceğin cevabı sikeyim program çöktü");

                answer = answer.trim().to_owned(); // Evinden dışlanmış string'ime bir ev veriyorum :'3
            
                if !a_indexes.contains(&&answer[..]) || &answer == "" {
                    println!("Geçersiz yanıt: \"{}\".", &&answer[..]);
                    continue
                }

                break // Verilen cevap geçerli. Cevap isteme döngüsünden çık.
            }
        
        for i in a_indexes.as_bytes() {
            if answer.as_bytes() == &[*i] {
                let add = randq.1[*i as usize - 97].1; // Saçma bir "workaround". "a", "b", "c" ve "d"nin byte değerleri sırasıyla 97, 98, 99, 100
                result += add;
                break
            }
        }

        i += 1;
    }
    
    show_result(result);
}


fn random_index(indexes: &mut Vec<u8>) -> u8 { // Kendisine verilen sayılar vektöründen rastgele bir sayıyı seçmek için bir fonksiyon.
    let a = rand::thread_rng().gen_range(0..=indexes.len() - 1); // Geçerli rastgele bir integer oluştur.
    let index = indexes[a];

    indexes.remove(a);
    
    return index;
}


fn show_result(r: i8) { // Sonucu bul ve göster.
    println!("\n{}",
        match r.cmp(&6) {
            Ordering::Less => "Solcu değilsin. Muhtemelen.",
            Ordering::Equal => "Solcusun.",
            Ordering::Greater => "Solcunun âlâsısın."
        }
    );
    
    io::stdin() // Kapanmak için girdi bekle.
        .read_line(&mut String::new())
        .expect("nasıl bir bok yedin bilmiyorum ama cevap vermen bile gerekmeyen bir yerde verdiğin cevapla programı çökertmiş bulundun. tebrikler");
}